"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var AppComponent = /** @class */ (function () {
    function AppComponent(go) {
        this.go = go;
    }
    // Aller à l'accueil
    AppComponent.prototype.goAccueil = function () {
        this.go.navigate(['/accueil']);
    };
    // Voir la liste
    AppComponent.prototype.goNews = function () {
        this.go.navigate(['/news']);
    };
    // Se connecter
    AppComponent.prototype.goConne = function () {
        this.go.navigate(['/conne']);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "ns-app",
            templateUrl: "app.component.html"
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsc0RBQStEO0FBTS9EO0lBQ0ksc0JBQW9CLEVBQW1CO1FBQW5CLE9BQUUsR0FBRixFQUFFLENBQWlCO0lBRXZDLENBQUM7SUFDRCxvQkFBb0I7SUFDcEIsZ0NBQVMsR0FBVDtRQUNJLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBQ0QsZ0JBQWdCO0lBQ2hCLDZCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUNELGVBQWU7SUFDZiw4QkFBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFmUSxZQUFZO1FBSnhCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsb0JBQW9CO1NBQ3BDLENBQUM7eUNBRXlCLHlCQUFnQjtPQUQ5QixZQUFZLENBZ0J4QjtJQUFELG1CQUFDO0NBQUEsQUFoQkQsSUFnQkM7QUFoQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJucy1hcHBcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcImFwcC5jb21wb25lbnQuaHRtbFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBnbzpSb3V0ZXJFeHRlbnNpb25zKXtcclxuXHJcbiAgICB9XHJcbiAgICAvLyBBbGxlciDDoCBsJ2FjY3VlaWxcclxuICAgIGdvQWNjdWVpbCgpe1xyXG4gICAgICAgIHRoaXMuZ28ubmF2aWdhdGUoWycvYWNjdWVpbCddKTtcclxuICAgIH1cclxuICAgIC8vIFZvaXIgbGEgbGlzdGVcclxuICAgIGdvTmV3cygpe1xyXG4gICAgICAgIHRoaXMuZ28ubmF2aWdhdGUoWycvbmV3cyddKTtcclxuICAgIH1cclxuICAgIC8vIFNlIGNvbm5lY3RlclxyXG4gICAgZ29Db25uZSgpe1xyXG4gICAgICAgIHRoaXMuZ28ubmF2aWdhdGUoWycvY29ubmUnXSk7XHJcbiAgICB9XHJcbn1cclxuIl19