import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {

    titre:string = "Native news";
    description:string = "Une application de démonstration pour la manipulation de NativeScript avec Angular."

    constructor(private go:RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        console.log("Accueil chargé");
    }
    // Aller à l'accueil
    goAccueil(){
        this.go.navigate(['/accueil']);
    }
    // Voir la liste
    goNews(){
        this.go.navigate(['/news']);
    }
    // Se connecter
    goConne(){
        this.go.navigate(['/conne']);
    }
}
