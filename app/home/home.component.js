"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(go) {
        this.go = go;
        this.titre = "Native news";
        this.description = "Une application de démonstration pour la manipulation de NativeScript avec Angular.";
        // Use the component constructor to inject providers.
    }
    HomeComponent.prototype.ngOnInit = function () {
        console.log("Accueil chargé");
    };
    // Aller à l'accueil
    HomeComponent.prototype.goAccueil = function () {
        this.go.navigate(['/accueil']);
    };
    // Voir la liste
    HomeComponent.prototype.goNews = function () {
        this.go.navigate(['/news']);
    };
    // Se connecter
    HomeComponent.prototype.goConne = function () {
        this.go.navigate(['/conne']);
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ["./home.component.css"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxzREFBK0Q7QUFRL0Q7SUFLSSx1QkFBb0IsRUFBbUI7UUFBbkIsT0FBRSxHQUFGLEVBQUUsQ0FBaUI7UUFIdkMsVUFBSyxHQUFVLGFBQWEsQ0FBQztRQUM3QixnQkFBVyxHQUFVLHFGQUFxRixDQUFBO1FBR3RHLHFEQUFxRDtJQUN6RCxDQUFDO0lBRUQsZ0NBQVEsR0FBUjtRQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBQ0Qsb0JBQW9CO0lBQ3BCLGlDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELGdCQUFnQjtJQUNoQiw4QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFDRCxlQUFlO0lBQ2YsK0JBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBdkJRLGFBQWE7UUFOekIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1lBQ3BDLFNBQVMsRUFBRSxDQUFDLHNCQUFzQixDQUFDO1NBQ3RDLENBQUM7eUNBTXlCLHlCQUFnQjtPQUw5QixhQUFhLENBd0J6QjtJQUFELG9CQUFDO0NBQUEsQUF4QkQsSUF3QkM7QUF4Qlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcIkhvbWVcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2hvbWUuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wiLi9ob21lLmNvbXBvbmVudC5jc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHRpdHJlOnN0cmluZyA9IFwiTmF0aXZlIG5ld3NcIjtcclxuICAgIGRlc2NyaXB0aW9uOnN0cmluZyA9IFwiVW5lIGFwcGxpY2F0aW9uIGRlIGTDqW1vbnN0cmF0aW9uIHBvdXIgbGEgbWFuaXB1bGF0aW9uIGRlIE5hdGl2ZVNjcmlwdCBhdmVjIEFuZ3VsYXIuXCJcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGdvOlJvdXRlckV4dGVuc2lvbnMpIHtcclxuICAgICAgICAvLyBVc2UgdGhlIGNvbXBvbmVudCBjb25zdHJ1Y3RvciB0byBpbmplY3QgcHJvdmlkZXJzLlxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiQWNjdWVpbCBjaGFyZ8OpXCIpO1xyXG4gICAgfVxyXG4gICAgLy8gQWxsZXIgw6AgbCdhY2N1ZWlsXHJcbiAgICBnb0FjY3VlaWwoKXtcclxuICAgICAgICB0aGlzLmdvLm5hdmlnYXRlKFsnL2FjY3VlaWwnXSk7XHJcbiAgICB9XHJcbiAgICAvLyBWb2lyIGxhIGxpc3RlXHJcbiAgICBnb05ld3MoKXtcclxuICAgICAgICB0aGlzLmdvLm5hdmlnYXRlKFsnL25ld3MnXSk7XHJcbiAgICB9XHJcbiAgICAvLyBTZSBjb25uZWN0ZXJcclxuICAgIGdvQ29ubmUoKXtcclxuICAgICAgICB0aGlzLmdvLm5hdmlnYXRlKFsnL2Nvbm5lJ10pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==