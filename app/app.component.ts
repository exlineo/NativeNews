import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent {
    constructor(private go:RouterExtensions){

    }
    // Aller à l'accueil
    goAccueil(){
        this.go.navigate(['/accueil']);
    }
    // Voir la liste
    goNews(){
        this.go.navigate(['/news']);
    }
    // Se connecter
    goConne(){
        this.go.navigate(['/conne']);
    }
}
