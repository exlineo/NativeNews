import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { HomeComponent } from "~/home/home.component";
import { NewslisteComponent } from "~/newsliste/newsliste.component";
import { ConnexionComponent } from "~/connexion/connexion.component";

const routes: Routes = [
    { path: "", redirectTo: "/accueil", pathMatch: "full" },
    { path: "accueil", component:HomeComponent },
    { path: "news", component:NewslisteComponent },
    { path: "conne", component:ConnexionComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
