import { Component, OnInit } from "@angular/core";
import * as Camera from "nativescript-camera";

import { News } from "../modele/news";

@Component({
    selector: "Newsliste",
    moduleId: module.id,
    templateUrl: "./newsliste.component.html",
    styleUrls: ["./newsliste.component.css"]
})
export class NewslisteComponent implements OnInit {
    titre:string = "Une liste de news"; // Le titre de la page
    entete:string = "res://stupidn"; // Une image d'entête
    listeNews:Array<News>; // Liste des news à afficher
    fakeNews:any; // Photo prise par l'appareil

    constructor() {
        this.listeNews = [
            {text:"Fake news", img:"res://donald", descr:"Wow! The NSA has deleted 685 million phone calls and text messages. Privacy violations? They blame technical irregularities. Such a disgrace. The Witch Hunt continues!"},
            {text:"News", img:"res://donald", descr:"Now that Harley-Davidson is moving part of its operation out of the U.S., my Administration is working with other Motor Cycle companies who want to move into the U.S. Harley customers are not happy with their move - sales are down 7% in 2017. The U.S. is where the Action is!"},
            {text:"News", img:"res://donald", descr:"Many good conversations with North Korea-it is going well! In the meantime, no Rocket Launches or Nuclear Testing in 8 months. All of Asia is thrilled. Only the Opposition Party, which includes the Fake News, is complaining. If not for me, we would now be at War with North Korea!"},
            {text:"News", img:"res://donald", descr:""}];
    }
    ngOnInit(): void {
        console.log("liste chargée");
        /* ***********************************************************
        * Use the "ngOnInit" handler to initialize data for this component.
        *************************************************************/
    }
    takeAPhoto(){
        Camera.requestPermissions();
        Camera.takePicture().then(photo => {
            this.fakeNews = photo;
            console.log(this.fakeNews);
        });
    }
}
