"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Camera = require("nativescript-camera");
var NewslisteComponent = /** @class */ (function () {
    function NewslisteComponent() {
        this.titre = "Une liste de news"; // Le titre de la page
        this.entete = "res://stupidn"; // Une image d'entête
        this.listeNews = [
            { text: "Fake news", img: "res://donald", descr: "Wow! The NSA has deleted 685 million phone calls and text messages. Privacy violations? They blame technical irregularities. Such a disgrace. The Witch Hunt continues!" },
            { text: "News", img: "res://donald", descr: "Now that Harley-Davidson is moving part of its operation out of the U.S., my Administration is working with other Motor Cycle companies who want to move into the U.S. Harley customers are not happy with their move - sales are down 7% in 2017. The U.S. is where the Action is!" },
            { text: "News", img: "res://donald", descr: "Many good conversations with North Korea-it is going well! In the meantime, no Rocket Launches or Nuclear Testing in 8 months. All of Asia is thrilled. Only the Opposition Party, which includes the Fake News, is complaining. If not for me, we would now be at War with North Korea!" },
            { text: "News", img: "res://donald", descr: "" }
        ];
    }
    NewslisteComponent.prototype.ngOnInit = function () {
        console.log("liste chargée");
        /* ***********************************************************
        * Use the "ngOnInit" handler to initialize data for this component.
        *************************************************************/
    };
    NewslisteComponent.prototype.takeAPhoto = function () {
        var _this = this;
        Camera.requestPermissions();
        Camera.takePicture().then(function (photo) {
            _this.fakeNews = photo;
            console.log(_this.fakeNews);
        });
    };
    NewslisteComponent = __decorate([
        core_1.Component({
            selector: "Newsliste",
            moduleId: module.id,
            templateUrl: "./newsliste.component.html",
            styleUrls: ["./newsliste.component.css"]
        }),
        __metadata("design:paramtypes", [])
    ], NewslisteComponent);
    return NewslisteComponent;
}());
exports.NewslisteComponent = NewslisteComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3c2xpc3RlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5ld3NsaXN0ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFDbEQsNENBQThDO0FBVTlDO0lBTUk7UUFMQSxVQUFLLEdBQVUsbUJBQW1CLENBQUMsQ0FBQyxzQkFBc0I7UUFDMUQsV0FBTSxHQUFVLGVBQWUsQ0FBQyxDQUFDLHFCQUFxQjtRQUtsRCxJQUFJLENBQUMsU0FBUyxHQUFHO1lBQ2IsRUFBQyxJQUFJLEVBQUMsV0FBVyxFQUFFLEdBQUcsRUFBQyxjQUFjLEVBQUUsS0FBSyxFQUFDLHlLQUF5SyxFQUFDO1lBQ3ZOLEVBQUMsSUFBSSxFQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUMsY0FBYyxFQUFFLEtBQUssRUFBQyxxUkFBcVIsRUFBQztZQUM5VCxFQUFDLElBQUksRUFBQyxNQUFNLEVBQUUsR0FBRyxFQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUMsMFJBQTBSLEVBQUM7WUFDblUsRUFBQyxJQUFJLEVBQUMsTUFBTSxFQUFFLEdBQUcsRUFBQyxjQUFjLEVBQUUsS0FBSyxFQUFDLEVBQUUsRUFBQztTQUFDLENBQUM7SUFDckQsQ0FBQztJQUNELHFDQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdCOztzRUFFOEQ7SUFDbEUsQ0FBQztJQUNELHVDQUFVLEdBQVY7UUFBQSxpQkFNQztRQUxHLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzVCLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxLQUFLO1lBQzNCLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXpCUSxrQkFBa0I7UUFOOUIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQzNDLENBQUM7O09BQ1csa0JBQWtCLENBMEI5QjtJQUFELHlCQUFDO0NBQUEsQUExQkQsSUEwQkM7QUExQlksZ0RBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0ICogYXMgQ2FtZXJhIGZyb20gXCJuYXRpdmVzY3JpcHQtY2FtZXJhXCI7XG5cbmltcG9ydCB7IE5ld3MgfSBmcm9tIFwiLi4vbW9kZWxlL25ld3NcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiTmV3c2xpc3RlXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL25ld3NsaXN0ZS5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogW1wiLi9uZXdzbGlzdGUuY29tcG9uZW50LmNzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBOZXdzbGlzdGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHRpdHJlOnN0cmluZyA9IFwiVW5lIGxpc3RlIGRlIG5ld3NcIjsgLy8gTGUgdGl0cmUgZGUgbGEgcGFnZVxuICAgIGVudGV0ZTpzdHJpbmcgPSBcInJlczovL3N0dXBpZG5cIjsgLy8gVW5lIGltYWdlIGQnZW50w6p0ZVxuICAgIGxpc3RlTmV3czpBcnJheTxOZXdzPjsgLy8gTGlzdGUgZGVzIG5ld3Mgw6AgYWZmaWNoZXJcbiAgICBmYWtlTmV3czphbnk7IC8vIFBob3RvIHByaXNlIHBhciBsJ2FwcGFyZWlsXG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5saXN0ZU5ld3MgPSBbXG4gICAgICAgICAgICB7dGV4dDpcIkZha2UgbmV3c1wiLCBpbWc6XCJyZXM6Ly9kb25hbGRcIiwgZGVzY3I6XCJXb3chIFRoZSBOU0EgaGFzIGRlbGV0ZWQgNjg1IG1pbGxpb24gcGhvbmUgY2FsbHMgYW5kIHRleHQgbWVzc2FnZXMuIFByaXZhY3kgdmlvbGF0aW9ucz8gVGhleSBibGFtZSB0ZWNobmljYWwgaXJyZWd1bGFyaXRpZXMuIFN1Y2ggYSBkaXNncmFjZS4gVGhlIFdpdGNoIEh1bnQgY29udGludWVzIVwifSxcbiAgICAgICAgICAgIHt0ZXh0OlwiTmV3c1wiLCBpbWc6XCJyZXM6Ly9kb25hbGRcIiwgZGVzY3I6XCJOb3cgdGhhdCBIYXJsZXktRGF2aWRzb24gaXMgbW92aW5nIHBhcnQgb2YgaXRzIG9wZXJhdGlvbiBvdXQgb2YgdGhlIFUuUy4sIG15IEFkbWluaXN0cmF0aW9uIGlzIHdvcmtpbmcgd2l0aCBvdGhlciBNb3RvciBDeWNsZSBjb21wYW5pZXMgd2hvIHdhbnQgdG8gbW92ZSBpbnRvIHRoZSBVLlMuIEhhcmxleSBjdXN0b21lcnMgYXJlIG5vdCBoYXBweSB3aXRoIHRoZWlyIG1vdmUgLSBzYWxlcyBhcmUgZG93biA3JSBpbiAyMDE3LiBUaGUgVS5TLiBpcyB3aGVyZSB0aGUgQWN0aW9uIGlzIVwifSxcbiAgICAgICAgICAgIHt0ZXh0OlwiTmV3c1wiLCBpbWc6XCJyZXM6Ly9kb25hbGRcIiwgZGVzY3I6XCJNYW55IGdvb2QgY29udmVyc2F0aW9ucyB3aXRoIE5vcnRoIEtvcmVhLWl0IGlzIGdvaW5nIHdlbGwhIEluIHRoZSBtZWFudGltZSwgbm8gUm9ja2V0IExhdW5jaGVzIG9yIE51Y2xlYXIgVGVzdGluZyBpbiA4IG1vbnRocy4gQWxsIG9mIEFzaWEgaXMgdGhyaWxsZWQuIE9ubHkgdGhlIE9wcG9zaXRpb24gUGFydHksIHdoaWNoIGluY2x1ZGVzIHRoZSBGYWtlIE5ld3MsIGlzIGNvbXBsYWluaW5nLiBJZiBub3QgZm9yIG1lLCB3ZSB3b3VsZCBub3cgYmUgYXQgV2FyIHdpdGggTm9ydGggS29yZWEhXCJ9LFxuICAgICAgICAgICAge3RleHQ6XCJOZXdzXCIsIGltZzpcInJlczovL2RvbmFsZFwiLCBkZXNjcjpcIlwifV07XG4gICAgfVxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhcImxpc3RlIGNoYXJnw6llXCIpO1xuICAgICAgICAvKiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAqIFVzZSB0aGUgXCJuZ09uSW5pdFwiIGhhbmRsZXIgdG8gaW5pdGlhbGl6ZSBkYXRhIGZvciB0aGlzIGNvbXBvbmVudC5cbiAgICAgICAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgICB9XG4gICAgdGFrZUFQaG90bygpe1xuICAgICAgICBDYW1lcmEucmVxdWVzdFBlcm1pc3Npb25zKCk7XG4gICAgICAgIENhbWVyYS50YWtlUGljdHVyZSgpLnRoZW4ocGhvdG8gPT4ge1xuICAgICAgICAgICAgdGhpcy5mYWtlTmV3cyA9IHBob3RvO1xuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5mYWtlTmV3cyk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==